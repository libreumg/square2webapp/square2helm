#!/bin/bash
# brew install kompose
# https://github.com/kubernetes/kompose/issues/1164#issuecomment-525898166
# docker-compose config > docker-compose-resolved.yaml && kompose convert -f docker-compose-resolved.yaml --volumes=configMap
#kompose -v convert --volumes=hostPath
#kompose -v convert -c --volumes=configMap 
# --volumes=persistentVolumeClaim

kompose -v convert -c --error-on-warning -o square2helm  --controller statefulset

